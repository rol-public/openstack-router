# frozen_string_literal: true

require 'fog/openstack'

# This is the library maim module.
module OpenStackRouter
end

require_relative 'openstack-router/version'
require_relative 'openstack-router/parameter/connection'
require_relative 'openstack-router/region'
require_relative 'openstack-router/catalog'
require_relative 'openstack-router/router'
