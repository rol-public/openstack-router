# frozen_string_literal: true

require 'securerandom'

module OpenStackRouter
  # The class responsible to detect the allowed endpoints (= regions) for
  # connection parameters.
  #
  # @since v0.2.0
  # @author Roland Laurès
  # @attr_reader [Parameter::Connection] connection_parameters
  #   The connection parameters.
  #
  # @todo finish description of the next attribute.
  # @attr_reader [Array<String>] endpoints The regions object used to
  #   manage the by region stuffs.
  class Catalog
    attr_reader :connection_parameters
    attr_reader :endpoints

    def initialize(connection_parameters)
      unless connection_parameters.class == Parameter::Connection
        raise ArgumentError, 'Invalid connection parameters. Please use OpenStackRouter::Parameter::Connection'
      end

      @connection_parameters = connection_parameters
      @token = Fog::OpenStack::Auth::Token.build(@connection_parameters.to_os_h, {})
      @endpoints = {}
      build_endpoints_hierarchy
    end

    def catalog
      token.catalog.payload.dup
    end

    def region_ids(interface = 'public')
      return [] unless @endpoints.key?(interface)

      @endpoints[interface].keys
    end

    private

    attr_reader :connection
    attr_reader :token

    # I want to be able to explore from ('interface') / region_id / service
    # TODO: It should manage users that can manipulate several project...
    # I have no instance of OpenStack to create such users. So maybe if someone can.
    def build_endpoints_hierarchy
      catalog.each do |service|
        service_type = service['type']
        service['endpoints'].each do |service_endpoint|
          append_endpoint(service_endpoint, service_type)
        end
      end
    end

    def append_endpoint(service_endpoint, service_type)
      interface = service_endpoint['interface']
      region_id = service_endpoint['region_id']
      endpoint_path(interface, region_id, service_type).append(service_endpoint)
    end

    # create or get and return corresponding endpoint path
    def endpoint_path(interface, region_id, service_type)
      ep_interface = @endpoints[interface]
      if ep_interface.nil?
        @endpoints[interface] = ep_interface = { region_id => { service_type => [] } }
      elsif !@endpoints[interface].key?(region_id)
        ep_interface[region_id] = { service_type => [] }
      elsif !@endpoints[interface][region_id].key?(service_type)
        ep_interface[region_id][service_type] = []
      end
      ep_interface[region_id][service_type]
    end
  end
end
