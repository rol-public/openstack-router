# frozen_string_literal: true

module OpenStackRouter
  # The class abstracting a region.
  # It aims to centralize a region state/status regarding VM spwning capacity
  # and connectivity.
  #
  # @author Roland Laurès
  # @attr_reader [String] name The region name.
  # @attr_reader [Symbol] state The region's state. @see {Region::STATES} descriptions
  # @attr_reader [Parameter::Connection] connection_parameters The parameters to connect to OpenStack
  #   services.
  class Region
    # The possible states of a Region
    STATES = [
      # connected and fully operational
      STATE_OPERATING = :operating,
      # connected but some limits has been reached
      STATE_LIMITED = :limited,
      # some connection problems occured
      STATE_FAILED = :failed
    ].freeze
    attr_reader :state
    attr_reader :connection_parameters
    attr_reader :name

    def initialize(name, connection_parameters)
      @name = name
      @connection_parameters = Parameter::Connection.new(connection_parameters.to_h.merge(region: name))

      @services = {}
      @state = STATE_OPERATING
    end

    # This method allow to call a request on a service for this region.
    # If the service hasn't been already connected it will be connected.
    #
    # @param [Symbol] service The service to contact (:compute, :image...)
    # @param [symbol] request_name The request to do on that service.
    #   It must exist else an exception will be raised.
    # @param [Array] args All other arguments will be passed to the requested
    #   method.
    #
    # @return [NilClass | 'a | Excon::Response]
    #   * nil if a SocketError occurs and
    #     the region cannot be reached.
    #   * +'a+ is the type returned by the block you give
    #   * Excon::Response is return if no block is given and no error is raised.
    #
    # @yield [answer] Block called with the answer for you to filter.
    #
    # @yieldparam [Excon::Response | Fog::Errors::NotFound] answer The answer of
    #   the request.
    #   @note Be aware that the answer of the server can be NotFound, because it
    #     didn't the requested ressource. Treat the case in your block.
    # @yieldreturn ['a] You return what you want. It will be then return without
    #   being touched by the method.
    #   Since nothing is done after yielding, then you can use the +return+ keyword
    #   from the block instead of +break+. But for safety, we recommand using +break+.
    def request(service, request_name, *args, &block)
      service = service.to_s.capitalize
      return nil if @state == STATE_FAILED && setup(service) == STATE_FAILED

      do_request(service, request_name, *args, &block)
    end

    def my_args(request_args, region_names)
      request_args.map do |arg|
        next arg unless arg.class == Hash && arg.keys == region_names

        arg[@name]
      end
    end

    private

    def setup(service)
      @services[service] = Object.const_get(Fog::OpenStack.service_klass(service)).new(@connection_parameters.to_os_h)
      @state = STATE_OPERATING
    rescue Excon::Error
      @state = STATE_FAILED if @services.empty?
      STATE_FAILED
    end

    def do_request(service, request_name, *args)
      setup(service) unless @services.key?(service)
      answer = @services[service].send(request_name, *args)
      block_given? ? yield(answer) : answer
    rescue Fog::Errors::NotFound => e
      e
    rescue Excon::Error, Fog::Errors::Error => _e
      # TODO: differentiate Fog::Errors because NotFound should not be treated like
      # a API/service side error but a user error.
      @services.delete(service)
      @state = STATE_FAILED if @services.empty?
      nil
    end
  end
end
