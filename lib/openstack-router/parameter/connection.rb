# frozen_string_literal: true

module OpenStackRouter
  # Contain all the classes that will treat complexe parameters of other classes.
  module Parameter
    # This class is used to parameter the connection to OpenStack instance and services.
    # Its purpose is to encapsulate the connection parameters.
    #
    # @author Roland Laurès
    # @attr_reader [String] auth_url The OpenStack url of the auth system
    # @attr_reader [String | NilClass] username The username to use to connect to OpenStack
    #   @note You can have username or userid set as you prefere, but need at least one of them set.
    # @attr_reader [String | NilClass] userid The user_id that you have been given to connect
    #   to the OpenStack services
    #   @note You can have username or userid set as you prefere, but need at least one of them set.
    # @attr_reader [String] api_key The api key that you have been given to connect
    #   to the OpenStack services
    # @attr_reader [String | NilClass] region The region on which to connect.
    # @attr_reader [String | NilClass] project_id The Project ID that you want to limit your
    #   actions on the OpenStack services
    class Connection
      attr_reader :auth_url
      attr_reader :api_key
      attr_reader :username
      attr_reader :userid
      attr_reader :region
      attr_reader :project_id

      # The mandatory options needed to be pass in argument at initialization of that class.
      MANDATORY_OPTIONS = %i[auth_url api_key].freeze
      # Paired mandatory options (means that one of one pair must me present, not the other).
      PAIRED_MANDATORY_OPTIONS = [%i[username userid]].freeze
      # The options that are not mandatory for this object.
      OTHER_OPTIONS = %i[region project_id].freeze
      # Constant containing all the available options.
      ALL_OPTIONS = (MANDATORY_OPTIONS + PAIRED_MANDATORY_OPTIONS.flatten + OTHER_OPTIONS).freeze

      def initialize(options)
        mandatory_options(options)
        paired_mandatory_options(options)
        other_options(options)
      end

      # get the corresponding connection parameter hash with openstack_ prefix.
      def to_os_h
        to_h.map { |key, value| ["openstack_#{key}".to_sym, value] }.reject { |a| a.last.nil? }.to_h
      end

      # convert the connection parameters to hash
      def to_h
        ALL_OPTIONS.map { |key| [key, send(key)] }.to_h
      end

      # convert to string the options (first convert to Hash, then the Hash to String)
      def to_s
        to_h.to_s
      end

      # checks if an option is present.
      # This method prevent testing nil? over the attribute.
      #
      # @return [Boolean] true if present, false if not
      # @raise [ArgumentError] if @key is not in the available options
      def present?(key)
        raise ArgumentError, "unavailable option: #{key}" unless ALL_OPTIONS.include?(key)

        instance_variable_defined?("@#{key}".to_sym)
      end

      private

      attr_writer :auth_url
      attr_writer :api_key
      attr_writer :username
      attr_writer :userid
      attr_writer :region
      attr_writer :project_id

      def mandatory_options(cfg)
        missing_keys = []
        MANDATORY_OPTIONS.each do |key|
          if cfg.key? key
            send("#{key}=".to_sym, cfg[key])
          else
            missing_keys.append(key)
          end
        end
        raise ArgumentError, "Missing keys: #{missing_keys}" unless missing_keys.empty?
      end

      def paired_mandatory_options(cfg)
        missing_keys = []
        PAIRED_MANDATORY_OPTIONS.each do |key1, key2|
          if cfg.key? key1
            send("#{key1}=".to_sym, cfg[key1])
          elsif cfg.key? key2
            send("#{key2}=".to_sym, cfg[key2])
          else
            missing_keys.append([key1, key2])
          end
        end
        raise ArgumentError, "Missing paired keys: #{missing_keys}" unless missing_keys.empty?
      end

      def other_options(cfg)
        cfg.each do |key, value|
          send("#{key}=".to_sym, value) if OTHER_OPTIONS.include?(key)
        end
      end
    end
  end
end
