# frozen_string_literal: true

require 'securerandom'

module OpenStackRouter
  # The main class of the module.
  # It aims to be the uniq entry point to access all the OpenStack services
  # and all the regions.
  #
  # @note For now we are using the default endpoint type only: 'public'. We have
  #   to make the router able to managed parametirized endpoint type ('admin' or so).
  #
  # @author Roland Laurès
  # @attr_reader [Parameter::Connection] connection_parameters
  #   The connection parameters.
  # @attr_reader [Array<Region>] regions The regions object used to
  #   manage the by region stuffs.
  class Router
    attr_reader :connection_parameters
    attr_reader :regions

    def initialize(connection_parameters, region_ids = [])
      unless connection_parameters.class == Parameter::Connection
        raise ArgumentError, 'Invalid connection parameters. Please use OpenStackRouter::Parameter::Connection'
      end

      @connection_parameters = connection_parameters
      @rr_circuits = {}
      @catalog = Catalog.new(connection_parameters)
      @regions = setup_regions(region_ids)
    end

    # @return [Array<String>] the region names
    def region_ids
      @regions.map(&:name)
    end

    # This method do a request on a service for each region in the router.
    #
    # @param [String | Symbol] service The service you want to interogate
    #   Service name should be capitalized, but by precaution, it is converted to string and capitalized
    #   at each request. :compute will be converted to 'Compute'.
    # @param [Symbol] request This is the request you want to launch on the regions' service.
    #   This is a method call, and then must be a Symbol.
    # @param [...] request_args The remaining arguments are destinated to the request call and will be passed
    #   as is.
    #   @see https://github.com/fog/fog-openstack For more information on available services and requests name
    #
    # @return [Hash] The result of each region, the key being the region_id and the value of the
    #   result of Region#request with the given argument.
    #
    # @yield [region_id, answer] Block called for each region that answers
    # @yieldparam [String] region_id The name of the region for that answer
    # @yieldparam [Excon::Response] answer The answer to the request.
    # @yieldreturn ['a] the return of the block is stored in the hash as a value
    #   to the key that will be the region_id.
    def request_each(service, request, *request_args)
      # TODO: make the requests to be parallele to no do them sequentialy...
      Hash[@regions.map do |region|
        [
          region.name,
          region.request(service, request, *request_args) do |answer|
            next answer unless block_given?

            yield(region.name, answer)
          end
        ]
      end]
    end

    # Create an UUID for a round robin circuit.
    # This allow to identify a round robin "group" that you can use for a given request or
    # a group of requests. You manage the round robin group as you like.
    #
    # @return [String] A randomly generated UUID.
    def rr_create
      key = SecureRandom.uuid
      @rr_circuits[key] = nil
      key
    end

    # @return [NilClass | Region] the last used region for that round robin circuit.
    #   nil is return if the circuit hasn't been used yet.
    def rr_last_region(rr_uuid)
      raise ArgumentError, 'Invalid UUID given' unless @rr_circuits.key?(rr_uuid)

      @rr_circuits[rr_uuid]
    end

    # Run in the given circuit [rr_uuid] the request.
    # @param [String] rr_uuid The uuid of the usd circuit. It must have been generated with {rr_create}
    # @param [String | Symbol] service The service you want to interogate
    #   Service name should be capitalized, but by precaution, it is converted to string and capitalized
    #   at each request. :compute will be converted to 'Compute'.
    # @param [Symbol] request This is the request you want to launch on the regions' service.
    #   This is a method call, and then must be a Symbol.
    # @param [...] request_args The remaining arguments are destinated to the request call and will be passed
    #   as is.
    #   @see https://github.com/fog/fog-openstack For more information on available services and requests name
    #
    # @return ['a] The result of the selected region, the value of the
    #   result of Region#request with the given argument.
    #
    # @yield [region_id, answer] Block called over the result of the request.
    # @yieldparam [String] region_id The name of the region that has been elected to run the request
    # @yieldparam [Excon::Response] answer The answer to the request.
    # @yieldreturn ['a] the result value of the block is returned by the method itself.
    def rr_request(rr_uuid, service, request, *request_args)
      raise ArgumentError, 'Invalid UUID given' unless @rr_circuits.key?(rr_uuid)

      region = rr_next_region(rr_uuid)
      region.request(service, request, *region.my_args(request_args, region_ids)) do |answer|
        next answer unless block_given?

        yield(region.name, answer)
      end
    end

    private

    def setup_regions(region_ids)
      if region_ids.empty?
        @catalog.region_ids.map { |region_id| Region.new(region_id, @connection_parameters) }
      else
        region_ids.map do |region_id|
          @catalog.region_ids.include?(region_id) ? Region.new(region_id, @connection_parameters) : nil
        end
                  .reject(&:nil?)
      end
    end

    def rr_next_region(rr_uuid)
      last_idx = @rr_circuits[rr_uuid]
      n_regions = @regions.length
      @rr_circuits[rr_uuid] = if last_idx.nil? then SecureRandom.rand(n_regions)
                              else (@rr_circuits[rr_uuid] + 1).modulo(n_regions)
                              end
      @regions[@rr_circuits[rr_uuid]]
    end
  end
end
