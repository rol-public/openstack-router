# frozen_string_literal: true

module OpenStackRouter
  # The version number of this library
  VERSION = '0.2.1'
end
