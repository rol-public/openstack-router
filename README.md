# OpenStack Router

[![pipeline status](https://gitlab.com/rol_public/openstack-router/badges/master/pipeline.svg)](https://gitlab.com/rol_public/openstack-router/commits/master)
[![Gem Version](https://badge.fury.io/rb/openstack-router.svg)](https://badge.fury.io/rb/openstack-router)
[![coverage report](https://gitlab.com/rol_public/openstack-router/badges/master/coverage.svg)](https://gitlab.com/rol_public/openstack-router/commits/master)

This lib is an overlay/router of OpenStack services and region.
It's goal will not be to write a wrapper for the requests themself.

# Current state and goal

## Currently (what we have)

We can manipulate several regions through a single interface.
We can launch a request on all regions, or through "circuits" of roundrobin
operations. This means that you can have several ressources you want to be
created in a roundrobin way, but you don't want instances roundrobin mix with
volume ones for example, or maybe you need to create several kinds of instances
with different roundrobin circuits (allowing small servers or big servers to be
better splattered).
We can automatically detect the available regions (by using endpoints sorting).

## What we currently don't have

We cannot manage 'admin' or 'internal' interfaces. We only work over 'public'.
We certainly don't support multiple project users... This will probably fail on
the catalog detection and stuffs.

## v1 goal

- Allow to manage multiple regions with one interface
- Allow to interact simply between regions:
  - create resources where possible (with a round-robin scheme for example)
  - copy images from a region to all the other ...
- Create some helpers to get all the images available
- if possible get a mean to detect all the regions of the cluster.

## Outside the scope of this library

- Not a wrapper for the requests them-selfs.

# Install and usage

Install on your system:

    gem install openstack-router

Install through bundler:

    gem 'openstack-router', '~> 0.1'

To use the library:

```ruby
require 'openstack_router'

cp = OpenStackRouter::Parameter::Connection.new(
  auth_url: 'https://auth.cloud.ovh.net/v2.0/',
  username: 'test_username',
  api_key: 'test_api_key',
  tenant: 'test_tenant',
  project_id: 'test_project_id',
  region: 'SBG3'
)
# For now only region connection can be done.
region = OpenStackRouter::Region.new(@connection_parameters)
region.images
region.limits
```

# documentation

Here some links:

 - [The documentation](https://rol-public.gitlab.io/openstack-router/)
 - [The coverage](https://rol-public.gitlab.io/openstack-router/coverage)
 - [The code quality report](https://rol-public.gitlab.io/openstack-router/rubycritic/overview.html)
