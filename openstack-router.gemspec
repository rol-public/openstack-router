# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift lib unless $LOAD_PATH.include?(lib)

require 'openstack-router/version'

# rubocop:disable Metrics/BlockLength
Gem::Specification.new do |s|
  s.name        = 'openstack-router'
  s.version     = OpenStackRouter::VERSION
  s.date        = Time.now.utc.strftime('%Y-%m-%d')
  s.summary     = 'OpenStack router mechanism'
  s.description =
    'This lib is an overlay/router of OpenStack components and region.'
  s.authors     = ['Roland Laurès']
  s.email       = 'roland@rlaures.pro'
  s.files       = Dir.glob('{lib}/**/*') + %w[LICENSE README.md]
  s.homepage    = 'https://gitlab.com/rol-public/openstack-router'
  s.license     = 'GNU GPLv3'
  s.require_path = 'lib'
  s.metadata = {
    'bug_tracker_uri' => 'https://gitlab.com/rol-public/openstack-router/issues',
    'documentation_uri' => 'https://rol-public.gitlab.io/openstack-router',
    'homepage_uri' => 'https://gitlab.com/rol-public/openstack-router',
    'mailing_list_uri' => 'https://gitlab.com/rol-public/openstack-router/issues/service_desk',
    'source_code_uri' => 'https://gitlab.com/rol-public/openstack-router/tree/master',
    'wiki_uri' => 'https://gitlab.com/rol-public/openstack-router/wikis/home'
  }

  s.required_ruby_version = '>= 2.4'

  s.add_runtime_dependency 'fog-openstack', '~> 1.0'

  s.add_development_dependency 'bundler-audit', '~> 0.6.1'
  s.add_development_dependency 'dotenv', '~> 2.6.0'
  s.add_development_dependency 'pry', '~> 0.12'
  s.add_development_dependency 'pry-byebug', '~> 3.6'
  s.add_development_dependency 'rb-readline', '~> 0.5'
  s.add_development_dependency 'rspec', '~> 3.8'
  s.add_development_dependency 'rubocop', '~> 0.55'
  s.add_development_dependency 'rubycritic', '~> 3.5'
  s.add_development_dependency 'simplecov', '~> 0.16'
  s.add_development_dependency 'yard', '~> 0.9'
end
# rubocop:enable Metrics/BlockLength
