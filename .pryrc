#  frozen_string_literal: true

require 'dotenv/load'
require 'openstack_router'

os_secret = {
  auth_url: 'https://auth.cloud.ovh.net',
  api_key: ENV.fetch('OS_API_KEY'),
  project_id: ENV.fetch('OS_PROJECT_ID', nil)
}
begin
  os_secret[:userid] = ENV.fetch('OS_USERID')
rescue KeyError
  os_secret[:username] = ENV.fetch('OS_USERNAME')
end
OS_SECRET = os_secret.freeze

param_conn = OpenStackRouter::Parameter::Connection.new(OS_SECRET)
@conn = OpenStackRouter::Router.new(param_conn)
