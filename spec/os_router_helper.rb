#  frozen_string_literal: true

# This file aims to integrate your specific API test
# We will be using environment variables to do so.
#
# The spec_env.rb file will be deliberatly rejected from git to prevent you from
# sending your credentials over the depot.
# Take a look at the spec_env.example.rb file for all the declarations.

# For all the other tests, this file configure the fog-openstack to be using the
# mock definitions. This is what will be used in the automated test on gitlab
# to prevent test to depend on gitlab's network fiability.
require 'dotenv/load'

os_secret = {
  auth_url: 'https://auth.cloud.ovh.net',
  api_key: ENV.fetch('OS_API_KEY'),
  project_id: ENV.fetch('OS_PROJECT_ID', nil)
}
begin
  os_secret[:userid] = ENV.fetch('OS_USERID')
rescue KeyError
  os_secret[:username] = ENV.fetch('OS_USERNAME')
end
OS_SECRET = os_secret.freeze
REGION_IDS = ENV.fetch('OS_REGIONS', 'BHS3 DE1 GRA3 SGB3 UK1 WAW1').split(' ')

TESTING_MOCK = if ENV['TEST_ON_REAL_OPENSTACK'] == 'true'
                 puts '!! Warning: lots of test doesn\'t work outside of the mock context.'
                 puts '     |-> This is kind of a failure prediction.'
                 puts ''
                 false
               else
                 puts '!! Warning: Test are running in MOCK mode.'
                 puts '   If you want to completly use your API setting for real,'
                 puts '   set TEST_ON_REAL_OPENSTACK=true in your environment.'
                 puts ' |-> This is not an error.'
                 puts ''
                 Fog.mock!
                 true
               end.freeze
