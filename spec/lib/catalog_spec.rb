#  frozen_string_literal: true

RSpec.describe OpenStackRouter::Catalog do
  before(:context) do
    @connection_parameters = OpenStackRouter::Parameter::Connection.new(OS_SECRET)
    @catalog = OpenStackRouter::Catalog.new(@connection_parameters)
  end
  let(:connection_parameters) { @connection_parameters }
  let(:router) { @router }

  describe '#initialize' do
    subject { OpenStackRouter::Catalog.new(connection_parameters) }
    context 'bad kind of connection_parameters' do
      let(:connection_parameters) { @connection_parameters.to_h }

      it do
        expect { subject }.to raise_error(
          ArgumentError,
          'Invalid connection parameters. Please use OpenStackRouter::Parameter::Connection'
        )
      end
    end

    context 'valid arguments' do
      it { is_expected.to be }
      it { is_expected.to be_an_instance_of(OpenStackRouter::Catalog) }
    end
  end

  describe '#endpoints' do
    subject { @catalog.endpoints }

    it { is_expected.to be_an_instance_of(Hash) }
    it { is_expected.not_to be_empty }
  end

  describe '#region_ids' do
    subject { @catalog.region_ids }

    it { is_expected.to be_an_instance_of(Array) }
    it { is_expected.not_to be_empty }

    context 'bad interface parameter' do
      subject { @catalog.region_ids('inexisting_interface') }

      it { is_expected.to be_an_instance_of(Array) }
      it { is_expected.to be_empty }
    end
  end
end
