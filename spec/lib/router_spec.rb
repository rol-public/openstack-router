#  frozen_string_literal: true

require 'set'

RSpec.describe OpenStackRouter::Router do
  before(:context) do
    @connection_parameters = OpenStackRouter::Parameter::Connection.new(OS_SECRET)
    @region_ids = REGION_IDS
    @router = OpenStackRouter::Router.new(@connection_parameters, @region_ids)
  end
  let(:connection_parameters) { @connection_parameters }
  let(:region_ids) { @region_ids }
  let(:router) { @router }

  describe '#initialize' do
    subject(:router) { OpenStackRouter::Router.new(connection_parameters, region_ids) }
    context 'bad kind of connection_parameters' do
      let(:connection_parameters) { @connection_parameters.to_h }

      it do
        expect { subject }.to raise_error(
          ArgumentError,
          'Invalid connection parameters. Please use OpenStackRouter::Parameter::Connection'
        )
      end
    end

    context 'valid arguments' do
      it { is_expected.to be }

      context 'without region_ids' do
        let(:region_ids) { [] }

        it { is_expected.to be }
        describe 'router.region_ids.sort' do
          it { expect(router.region_ids.sort).to be_eql(REGION_IDS.sort) }
        end
      end

      context 'with region_ids that are not all present' do
        let(:inexisting_region_ids) { %w[TEST1 TEST2 TEST3] }
        let(:region_ids) { REGION_IDS[0..2].concat(inexisting_region_ids) }

        it { is_expected.to be }
        describe 'router.region_ids.to_set' do
          subject { router.region_ids.to_set }
          it { is_expected.to be_subset(region_ids.to_set) }
          it 'should not intersect #<Set: {"TEST1", "TEST2", "TEST3"}>' do
            is_expected.not_to be_intersect(inexisting_region_ids.to_set)
          end
        end
      end
    end
  end

  describe '#connection_parameters' do
    subject { router.connection_parameters }
    it { is_expected.to be_an_instance_of(OpenStackRouter::Parameter::Connection) }
  end

  describe '#regions' do
    subject { router.regions }
    it { is_expected.to be_an_instance_of(Array) }
    it { is_expected.to all(be_an(OpenStackRouter::Region)) }
  end

  describe '#region_ids' do
    subject { router.region_ids }
    it { is_expected.to eq(region_ids) }
  end

  describe '#request_each' do
    let(:service) { :compute }
    let(:request) { :get_limits }

    context 'without block' do
      subject { router.request_each(service, request) }

      it { is_expected.to be_an(Hash) }
      it { is_expected.to include(*region_ids) }
      it { expect(subject.values).to all(be_an(Excon::Response)) }
    end

    context 'with a block' do
      subject do
        router.request_each(service, request) do |region_id, answer|
          [region_id, answer.body['limits']]
        end
      end

      it { is_expected.to be_an(Hash) }
      it 'should give an answer for each region' do
        expect(subject.keys).to eq(region_ids)
      end
      it 'should have got the region_ids' do
        expect(subject.values.map(&:first)).to eq(region_ids)
      end
      it 'should have transmitted the result of each request' do
        expect(subject.values.map { |_, limit| limit }).to all(be_an(Hash))
      end
    end
  end

  describe 'roundroubin methods:' do
    describe '#rr_create' do
      subject { router.rr_create }
      it { is_expected.to be_an(String) }
      it { is_expected.to match(/\A[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}\z/) }
    end

    describe '#rr_last_region' do
      subject { router.rr_last_region(uuid) }

      context 'created circuit' do
        let(:uuid) { router.rr_create }

        context 'before first usage' do
          it { is_expected.to eq(nil) }
        end

        context 'after first usage' do
          it do
            router.rr_request(uuid, :compute, :get_limits)
            is_expected.to be
          end
        end
      end

      context 'not created circuit uuid' do
        let(:uuid) { SecureRandom.uuid }

        it { expect { subject }.to raise_error(ArgumentError, 'Invalid UUID given') }
        it { expect { router.rr_last_region('invalid') }.to raise_error(ArgumentError, 'Invalid UUID given') }
      end
    end

    describe '#rr_request' do
      before(:context) do
        @rr_uuid = @router.rr_create
      end
      let(:rr_uuid) { @rr_uuid }
      subject { @router.rr_request(rr_uuid, :compute, :get_limits) }

      it { is_expected.to be_an(Excon::Response) }
      it 'should change #rr_last_region value' do
        r0 = @router.rr_last_region(rr_uuid)
        subject
        expect(@router.rr_last_region(rr_uuid)).not_to eq(r0)
      end

      context 'unknown uuid' do
        let(:rr_uuid) { SecureRandom.uuid }

        it { expect { subject }.to raise_error(ArgumentError, 'Invalid UUID given') }
        it do
          expect { @router.rr_request('invalid', :compute, :get_limits) }.to(
            raise_error(ArgumentError, 'Invalid UUID given')
          )
        end
      end
    end
  end
end
