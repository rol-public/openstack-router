#  frozen_string_literal: true

RSpec.describe OpenStackRouter::Parameter::Connection do
  let(:default_options) do
    {
      auth_url: 'https://auth.cloud.ovh.net',
      api_key: 'test_api_key',
      userid: 'test_userid'
    }
  end
  let(:options) { default_options }

  describe '#initialize' do
    subject { OpenStackRouter::Parameter::Connection.new(options) }
    context 'missing one option' do
      let(:options) { default_options.reject { |k, _| k == :api_key } }
      let(:error_message) { 'Missing keys: [:api_key]' }

      it { expect { subject }.to raise_error(ArgumentError, error_message) }
    end

    context 'paired options' do
      context 'missing both' do
        let(:options) { default_options.reject { |k, _| k == :userid } }
        let(:error_message) { 'Missing paired keys: [[:username, :userid]]' }

        it { expect { subject }.to raise_error(ArgumentError, error_message) }
      end
    end

    context 'valid arguments' do
      it { is_expected.to be }
    end
  end

  describe '#to_os_h' do
    let(:options) { default_options.merge(region: 'SBG3') }
    let(:options_remade) do
      options.map { |k, v| ["openstack_#{k}".to_sym, v] }.to_h
    end
    subject { OpenStackRouter::Parameter::Connection.new(options).to_os_h }

    it { is_expected.to be_an_instance_of(Hash) }
    it { is_expected.to eq(options_remade) }
  end

  describe '#present?' do
    subject { OpenStackRouter::Parameter::Connection.new(options).present?(tested_value) }

    context 'mandatory option' do
      let(:tested_value) { :userid }
      it { is_expected.to be_truthy }
    end

    context 'on optional' do
      let(:options) { default_options.merge(region: 'SBG3') }

      context 'present option' do
        let(:tested_value) { :region }
        it { is_expected.to be_truthy }
      end

      context 'absent option' do
        let(:tested_value) { :project_id }
        it { is_expected.to be_falsy }
      end
    end

    context 'inexisting option' do
      let(:tested_value) { :unexisting_key }
      it { expect { subject }.to raise_error(ArgumentError, 'unavailable option: unexisting_key') }
    end
  end
end
