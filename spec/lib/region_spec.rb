#  frozen_string_literal: true

RSpec.describe OpenStackRouter::Region do
  before(:context) do
    @connection_parameters = OpenStackRouter::Parameter::Connection.new(
      auth_url: 'https://auth.cloud.ovh.net',
      api_key: 'test_api_key',
      userid: 'test_userid',
      project_id: 'test_project_id'
    )
    @region_name = 'SBG3'
    @region = OpenStackRouter::Region.new(@region_name, @connection_parameters)
  end
  let(:connection_parameters) { @connection_parameters }
  let(:region_name) { @region_name }

  describe '#initialize' do
    subject { OpenStackRouter::Region.new(region_name, connection_parameters) }
    context 'any hash connection_parameters' do
      let(:connection_parameters) { @connection_parameters.to_h }

      it { is_expected.to be }
    end
  end

  describe '#request' do
    subject { @region.request(*request_args) }

    context 'connected and without a block' do
      let(:request_args) { %i[compute get_limits] }
      it { is_expected.to be_an_instance_of(Excon::Response) }
    end

    context 'connected and with a block' do
      subject(:do_request) do
        @region.request(:compute, :get_limits) do |answer|
          answer.body['limits']
        end
      end
      it { expect(do_request).to be_an_instance_of(Hash) }
      it { expect(do_request).to include('absolute') }
    end
  end

  describe '#my_args' do
    let(:request_args) do
      [
        'a_server_name',
        { 'SBG3' => 'SBG3_flavorRef', 'WAW1' => 'WAW1_flavorRef' },
        { 'SBG3' => 'SBG3_imageRef', 'WAW1' => 'WAW1_imageRef' },
        { 'otheroption1' => 'value1', 'otheroption2' => 'value2', 'SBG3' => 'avoid collision' }
      ]
    end
    subject { @region.my_args(request_args, %w[SBG3 WAW1]) }
    it do
      is_expected.to eq(
        [
          'a_server_name', 'SBG3_flavorRef', 'SBG3_imageRef',
          { 'otheroption1' => 'value1', 'otheroption2' => 'value2', 'SBG3' => 'avoid collision' }
        ]
      )
    end
  end
end
